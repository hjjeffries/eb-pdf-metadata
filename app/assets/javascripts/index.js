window.onload = function() {
  if (document.querySelector(".pages")) {
    const submitEl = document.getElementById("submitUrls");
    const urlTextarea = document.getElementById("urlsInput");
    const endpointTextEl = document.getElementById("endpointUrl");
    const copyUrlText = document.getElementById("copyEndpointUrl");
    const curlTextEl = document.getElementById("curlCommand");
    const copyCurlText = document.getElementById("copyCurlCommand");
    const host = document.getElementById("requestInfo").dataset.requestInfo;
    const jsonResults = document.getElementById("jsonResults");
    const spacer = "urls[]=";
    const baseUrl = host + "/pdf_metadata?"

    endpointTextEl.innerText = baseUrl + spacer;
    curlTextEl.innerText = 'curl "' + baseUrl + spacer + '"';

    urlTextarea.addEventListener("ionChange", function (e) {
      let sendUrl = baseUrl;
      let text = e.detail.value.split("\n");
      text.forEach(function(url, index) {
        if (index === 0) {
          sendUrl += (spacer + url);
        }
        else {
          sendUrl += ("&" + spacer + url) 
        }
      });
      
      endpointTextEl.innerText = sendUrl;
      curlTextEl.innerText = 'curl "' + sendUrl + '"';
    });

    copyUrlText.addEventListener("click", function(e) {
      copyText("endpointUrl")
      presentToast("Copied successfully");
    });

    copyCurlText.addEventListener("click", function (e) {
      copyText("curlCommand");
      presentToast("Copied successfully");
    });

    function copyText(source_id) {
      const el = document.createElement('textarea');
      source = document.getElementById(source_id);
      el.value = source.innerText;
      el.setAttribute('readonly', '');
      el.style.position = 'absolute';
      el.style.left = '-9999px';
      document.body.appendChild(el);
      el.select();
      document.execCommand('copy');
      document.body.removeChild(el);
    }

    submitEl.addEventListener("click", function (e) {
      presentLoading().then(
        submitRequest()
      );
    });
    
    function submitRequest() {
      const request = new XMLHttpRequest();
      request.onreadystatechange = function () {
        if (request.readyState == XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
          if (request.status == 200) {
            jsonResults.innerText = JSON.stringify(JSON.parse(request.responseText), null, 2);
          }
          else if (request.status == 400) {
            alert('There was an error 400');
          }
          else {
            alert('Sorry, something went wrong.');
          }
        dismissLoading();
        }
      };

      request.open("GET", endpointTextEl.innerText, true);
      request.send();
    }

    async function presentLoading() {
      const loadingController = document.querySelector('ion-loading-controller');
      await loadingController.componentOnReady();

      const loadingElement = await loadingController.create({
        message: 'Working on it...',
        spinner: 'crescent'
      });
      return await loadingElement.present();
      dismissLoading();
    }

    function dismissLoading() {
      const loader = document.querySelector('ion-loading');
      loader.dismiss();
    }
  }
}

