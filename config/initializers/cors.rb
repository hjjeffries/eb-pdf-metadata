# Allow CORS from any origin for the specific endpoint

Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins '*'
    resource '/pdf_metadata',
      headers: :any,
      methods: %i(get options head)
  end
end