# Returns sorted PDF metadata to spec based on an array of URLs to convert into PDFs
module PdfMetadata
  require "docraptor"     # DocRaptor gem
  require "pdf-reader"    # PDF-Reader gem
  require 'tempfile'      # Ruby native
  require 'json'          # Ruby native

  # Configure DocRaptor as necessary
  DocRaptor.configure do |dr|
    dr.username = "YOUR_API_KEY_HERE"
  end

  DOCRAPTOR = DocRaptor::DocApi.new

  # Main entry point
  # Process each URL to convert it into a PDF, collect PDF metadata, sort, and return JSON
  # The url param should be supplied as an array of urls. 
  def process_urls(urls=[""])                 
    # Note: At the risk of false-negatives, one could add better validation to URLs. The current
    # implementation optimistically assumes that URLs are well-formed and allows the DocRaptor API
    # to sort it out, simply removing nil results before further processing. 

    # Retrieve an array of metadata hashes
    results_arr = urls.collect { |url| return_pdf_metadata(pdf_result(url), url) }

    # Sort results according to the specified pattern
    sorted_results = sort_results(results_arr.compact)

    # Return JSON to the client
    sorted_results.to_json

  end

  # Use DocRaptor API to generate PDFs
  # Silently fail (returning nil) for any unprocessable url.
  def pdf_result(url)
    begin
      # https://docraptor.com/documentation/api#api_general
      create_response = DOCRAPTOR.create_doc(
        test:             true,                                         # test documents are free but watermarked
        document_url:     url, 
        name:             "docraptor-ruby.pdf",                         # help you find a document later
        document_type:    "pdf",                                        # pdf or xls or xlsx
      )

      # Produce a PDF file from which to extract metadata
      # Use temporary files, since storage is not required.
      temp_pdf = Tempfile.new(["temppdf", ".pdf"], Rails.root.join('public/pdf_tmp')).binmode
      temp_pdf << create_response

      temp_pdf     # Return the path to the temporary file
    rescue DocRaptor::ApiError => error
      puts "#{error.class}: #{error.message}"
      puts error.code          # HTTP response code
      puts error.response_body # HTTP response body
      puts error.backtrace[0..3].join("\n")
    end
  end

  # Use PDF-Reader to extract a single PDF's metadata and close the temporary file.
  # Silently fail if the data are malformed. 
  def return_pdf_metadata(temp_pdf, url)
    #     
    begin
      reader = PDF::Reader.new(temp_pdf.path)
      pdf_metadata_obj = {}
      pdf_metadata_obj["url"] = url
      pdf_metadata_obj["pdf_version"] = reader.pdf_version
      pdf_metadata_obj["info"] = reader.info
      pdf_metadata_obj["metadata"] = reader.metadata
      pdf_metadata_obj["page_count"] = reader.page_count
      temp_pdf.close
      pdf_metadata_obj
    rescue
    end
  end

  def sort_results(arr=[])
    hash = {}
    
    # Group metadata objects in arrays by page count within a hash
    arr.each do |obj|
      if hash.key?(obj["page_count"])
        hash[obj["page_count"]].push(obj)
      else
        hash[obj["page_count"]] = [].push(obj)
      end
    end

    # Sort the hash by page count (N.b. This was not explicitly requested, but assumed desirable.)
    ordered_hash = hash.sort.to_h

    # Sort each page count group alphabetically by URL
    ordered_hash.each do |key, value|
      value.sort_by! { |hsh| hsh["url"] }
    end
  end

end