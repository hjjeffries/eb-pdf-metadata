Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # post 'pdf_metadata/index', to: 'pdf_metadata#index', as: :pdf_metadata
  get '/pdf_metadata', to: 'pdf_metadata#index'

  root 'pages#index'
end
