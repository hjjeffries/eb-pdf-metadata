# Mr. Peedy Eff

Repo: https://bitbucket.org/hjjeffries/eb-pdf-metadata
Production: https://eb-pdf-metadata.herokuapp.com/

## Overview
This application is offered in response to a coding challenge. The app provides an endpoint that takes a list of URLs and returns information according to specifications using the DocRaptor API and PDF-Reader gem. The application also offers a small interface for testing and interacting with the endpoint.

## Usage
With a deployed instance of this app, submit URLs to the defined endpoint (/pdf_metadata) of the form

```
.../pdf_metadata?urls[]=http://domain.com&urls[]=...
```

Based on the supplied URLs, the app generates PDFs through DocRaptor's API and saves them as temporary files (file persistence was not part of the spec). It then extracts metadata from each PDF and returns JSON grouping results by page count and alphabetically within each page count group.

You can submit data in of these ways:
1. From the application index page
2. From the terminal using cURL
3. Directly at the endpoint URL

The index page has the benefit of providing properly formed URLs and pretty-printed JSON. All requests on the production app are constrained by Heroku's 30-second timeout (see discussion below). 

## Notes
I tried to make reasonable assumptions regarding what was important or salient for the exercise. I recognize several points where the app could be further developed, depending on an actual use case. Here are some comments on my assumptions, approach, and implementation.

* THROUGHPUT -- Throughput for this application varies by access method. Using cURL from localhost with no timeout, the bottleneck is only the DocRaptor API call for any one document (60 seconds using sync calls). But Heroku enforces a 30-second timeout on the production version for a single transaction, which includes the processing time for all URLs in a request. So any very large request will fail. Overall, I did not try to solve against this deployment-related constraint, but I do recognize that optimizations could (and should) be made at most points in the application and its deployment for greater throughput. Among these optimizations is the request method (sync/async) and server configuration. 
* VALIDATION -- Setting throughput aside, I prioritized the return of data over up-front validation--i.e. the app makes an effort to get whatever it can out of a submitted request and will silently fail on malformed items. In some use cases, the user would want a lot more information about what went wrong and how to fix it.
* CODE -- The heart of the code is found in a Ruby module (/lib/pdf_metadata.rb). My approach with the code was to make it as independent and reusable as possible within the specs of the exercise.

Written by James Jeffries, hjjeffries@gmail.com

