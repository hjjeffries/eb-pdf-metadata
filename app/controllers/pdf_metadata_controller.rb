class PdfMetadataController < ApplicationController
  include PdfMetadata

  def index
    @urls = params[:urls]
    
    respond_to do |format|
      if @urls.present?
        results = process_urls(@urls)
        format.html { 
          render json: results
        }
      else
        format.html { 
          render plain: "Please submit parameters of the form '/pdf_metadata?urls[]=http://domain.com&urls[]=...' to receive PDF metadata."
        }
      end
    end
  end

  private

  def pdf_params
    params.permit(:urls)
  end

end